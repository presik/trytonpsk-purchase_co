# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.modules.product import price_digits


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
    last_cost = fields.Function(fields.Numeric('Last Cost', digits=price_digits,
        help='Last purchase price'), 'get_last_cost')

    def get_last_cost(self, name=None):
        res = self.cost_price
        InvoiceLine = Pool().get('account.invoice.line')
        products = InvoiceLine.search_read([
            ('product', '=', self.id),
            ('invoice.state', 'in', ['posted', 'paid', 'validated']),
            ('invoice.type', '=', 'in'),
        ], fields_names=['unit_price'], order=[
            ('invoice.invoice_date', 'DESC')],
            limit=1)
        if products:
            res = products[0]['unit_price']
        return res
