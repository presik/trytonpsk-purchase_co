# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import purchase
from . import product
from . import account
from . import requisition


def register():
    Pool.register(
        account.PrintSupplierCertificateStart,
        product.Product,
        purchase.Configuration,
        purchase.Party,
        purchase.Purchase,
        purchase.Line,
        purchase.PurchaseAnalyticStart,
        purchase.PurchasesDetailedStart,
        purchase.PurchaseUpdateStart,
        module='purchase_co', type_='model')
    Pool.register(
        requisition.CreatePurchaseGroupBySupplierStart,
        module='purchase_co', type_='model', depends=['purchase_requisition'])
    Pool.register(
        account.PrintSupplierCertificate,
        purchase.PurchasesDetailed,
        purchase.PurchaseAnalytic,
        purchase.PurchaseForceDraft,
        purchase.PurchaseGenerateInvoice,
        purchase.PurchaseGenerateShipment,
        purchase.PurchaseUpdate,
        module='purchase_co', type_='wizard')
    Pool.register(
        requisition.CreatePurchaseGroupBySupplier,
        module='purchase_co', type_='wizard', depends=['purchase_requisition'])
    Pool.register(
        account.PrintSupplierCertificateReport,
        purchase.PurchasesDetailedReport,
        purchase.PurchaseAnalyticReport,
        module='purchase_co', type_='report')
