# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from itertools import chain

from trytond import backend
from trytond.i18n import gettext
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Bool, If, Id
from trytond.model import Workflow, ModelView, fields, ModelSQL, \
        sequence_ordered
from trytond.model.exceptions import RequiredValidationError, AccessError
from trytond.wizard import Wizard
from trytond.transaction import Transaction
from trytond.tools import grouped_slice, firstline
from trytond.wizard import Wizard, StateTransition, StateView, Button

from trytond.modules.company.model import (
    employee_field, set_employee, reset_employee)
from trytond.modules.product import price_digits, round_price


class CreatePurchaseGroupBySupplierStart(ModelView):
    'Create Purchase Group By Supplier Start'
    __name__ = 'purchase_co.create_purchase_group_by_supplier.start'
    supplier = fields.Many2One('party.party', 'Supplier', required=True)
    requests = fields.Many2Many('purchase.request', None, None,
                                'Request', domain=[('state', '=', 'draft')])


class CreatePurchaseGroupBySupplier(Wizard):
    'Create Purchase Group By Supplier'
    __name__ = 'purchase_co.create_purchase_group_by_supplier'

    start = StateView('purchase_co.create_purchase_group_by_supplier.start',
        'purchase_co.create_purchase_group_by_supplier_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('OK', 'create_purchase', 'tryton-ok', True),
            ])
    create_purchase = StateTransition()

    def transition_create_purchase(self):
        pool = Pool()
        party = self.start.supplier
        for request in self.start.requests:
            request.party = party
        CreatePurchase = pool.get('purchase.request.create_purchase', type='wizard')
        session_id, _, _ = CreatePurchase.create()
        CreatePurchase.records = self.start.requests
        CreatePurchase.execute(session_id, {}, 'start')
        CreatePurchase.delete(session_id)
        return 'end'
