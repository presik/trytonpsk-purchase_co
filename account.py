# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, Button, StateReport
from trytond.report import Report
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
from trytond.exceptions import UserError
from datetime import date

MONTHS = [
    'enero',
    'febrero',
    'marzo',
    'abril',
    'mayo',
    'junio',
    'julio',
    'agosto',
    'septiembre',
    'octubre',
    'noviembre',
    'diciembre'
]

conversor = None
try:
    from numword import numword_es
    conversor = numword_es.NumWordES()
except:
    print("Warning: Does not possible import numword module, please install it...!")


class InvoiceAuthorization(metaclass=PoolMeta):
    __name__ = 'account.invoice.authorization'

    # @classmethod
    # def __setup__(cls):
    #     super().__setup__()
    #     cls.kind.selection.append(('supplier_certificate', "Supplier Certificate"))


class PrintSupplierCertificateStart(ModelView):
    'Print Supplier Certificate Start'
    __name__ = 'purchase_co.print_supplier_certificate.start'
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscalyear',
        required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    party = fields.Many2One('party.party', 'Party')
    start_period = fields.Many2One('account.period', 'Start Period',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('start_date', '<=', (Eval('end_period'), 'start_date')),
            ], depends=['fiscalyear', 'end_period'])
    end_period = fields.Many2One('account.period', 'End Period',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('start_date', '>=', (Eval('start_period'), 'start_date'))
            ],
        depends=['fiscalyear', 'start_period'])
    description = fields.Char('Description')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class PrintSupplierCertificate(Wizard):
    'Print Supplier Certificate'
    __name__ = 'purchase_co.print_supplier_certificate'
    start = StateView('purchase_co.print_supplier_certificate.start',
        'purchase_co.print_supplier_certificate_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('purchase_co.print_supplier_certificate_report')

    def do_print_(self, action):
        if self.start.start_period:
            start_period = self.start.start_period.id
        else:
            start_period = None
        if self.start.end_period:
            end_period = self.start.end_period.id
        else:
            end_period = None

        party_id = None
        if self.start.party:
            party_id = self.start.party.id
        data = {
            'company': self.start.company.id,
            'fiscalyear': self.start.fiscalyear.id,
            'party': party_id,
            'start_period': start_period,
            'end_period': end_period,
            'description': self.start.description
            }
        return action, data


class PrintSupplierCertificateReport(Report):
    __name__ = 'purchase_co.print_supplier_certificate_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Period = pool.get('account.period')
        Party = pool.get('party.party')
        User = pool.get('res.user')
        Invoice = pool.get('account.invoice')
        company = Company(data['company'])
        start_period = None
        end_period = None
        dom_invoices = [
            ('party', '=', data['party']),
            ('type', '=', 'in'),
        ]
        if data['start_period']:
            start_period = Period(data['start_period'])
            dom_invoices.append(('invoice_date', '>=', start_period.start_date))

        if data['end_period']:
            end_period = Period(data['end_period'])
            dom_invoices.append(('invoice_date', '<=', end_period.end_date))

        invoices = Invoice.search(dom_invoices)
        total_amount = 0
        for invoice in invoices:
            print(invoice.untaxed_amount, invoice.number)
            total_amount += invoice.untaxed_amount

        digits = company.currency.digits
        user_id = Transaction().user
        report_context['num_in_words'] = cls.get_total_amount_words(total_amount, digits)
        report_context['total_amount'] = total_amount
        report_context['date_today'] = date.today()
        report_context['start_period'] = start_period.start_date
        report_context['end_period'] = end_period.start_date
        report_context['party'] = Party(data['party'])
        report_context['total_amount'] = total_amount
        report_context['company'] = company
        report_context['description'] = data['description']
        report_context['months'] = MONTHS
        report_context['user'] = User(user_id)
        return report_context

    @classmethod
    def get_total_amount_words(cls, total_amount, digits):
        if conversor and total_amount:
            digits = digits
            total_amount = (round(total_amount, digits))
            num = (conversor.cardinal(int(total_amount))).upper()
            return num
